# Tic-Tac-Toe Game
*Developed by Etienne Beauchemin-Pepin*
## What is this game?
This is a tic-tac-toe game playable by 2 players in a command line terminal. It is developed in python.
## How do I play this game?
### Installation
1. Clone or download this repository
1. Install Python 3.7
1. Open a terminal or command prompt window
    1. Run `pip install pipenv` to install the `pipenv` module in *Python*
    1. Run `pipenv install` in the directory of this *readme* to create the environment to run the game.
### Playing the game
1.  Run `pipenv run python .\ticTacToeGame\RunMe.py` from the folder of this *readme*.
## How do I develop this game?
### Install the development environment
1. Clone this repository
1. Install Python 3.7
1. Open a terminal or a command prompto:
    1. Run `pip install pipenv`
    1. Run `pipenv install --dev` from the folder where the `Pipfile` is located.
### Execute automated tests
#### Unit tests
Run `pipenv run python -m unittest discover -v -s .\ticTacToeGame\tests\unitTests\`
#### Scenario tests
Run `pipenv run python -m unittest discover -v -s .\ticTacToeGame\tests\scenarioTests\`
## Why develop yet another tic-tac-toe game?
There already exists many tic-tac-toe games out there and I don't think this will revolutionize the genre. However, as I was learning python, I needed concrete practice to improve my understanding and knowledge of the language.

Here are a few points that I focused on while developing this game:

* **Code quality**: single-purpose classes, low coupling, readable code
* **Intuitive player interactions**. The player shouldn't need a tutorial to get through the game.
* Experiment with **automated testing**:
    
    * **Unit tests** for main pieces of logic
    * **Scenario tests** to test the game from a user point of view. In this case, have tests interact through command line.
