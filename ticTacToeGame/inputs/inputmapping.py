from typing import Dict, List


class InputNotMappedError(Exception):
    pass


class TicTacToeInputMapping:
    def __init__(self, input_mapping: Dict[str, int]):
        """
        Inputs:
            input_mapping:  A mapping between an input text and a square index in the tictactoe board.
                            The input text is a string representing the text input a user has to feed to get a certain square index
        """

        # The mapping is a dict mapping some text to a square index on the board
        # It maps the numpad of a keyboard to a tictactoe board
        self.input_to_square_index: Dict[str, int] = input_mapping

        # Reverse the input mapping to be able to know which input for which square index
        square_index_to_input_list: Dict[int, str] = []
        for input_text, square_index in self.input_to_square_index.items():
            square_index_to_input_list.append((square_index, input_text))
        self.square_index_to_input = dict(square_index_to_input_list)

    def get_square_index(self, input_text: str) -> int:
        """ Get the index of the square mapped to the input or raise a InputNotMappedError"""
        try:
            return self.input_to_square_index[input_text]
        except KeyError:
            raise InputNotMappedError(
                f"No square index associated to input '{input_text}'"
            )

    def get_input_text(self, square_index: int) -> str:
        """ Get the input text that is mapped to a square index or raise a InputNotMappedError if the square_index is not found"""
        try:
            return self.square_index_to_input[square_index]
        except KeyError:
            raise InputNotMappedError(
                f"No input linked to the square index '{square_index}' was found."
            )

    def get_all_input_texts(self) -> List[str]:
        return list(self.input_to_square_index.keys())

    def get_inputs_texts(self, squares_indexes: List[int]) -> List[str]:
        """
        Get the input texts for the provided square, the list returned will be in the same order as the square indexes passed
        """
        result: List[str] = []

        for index in squares_indexes:
            result.append(self.get_input_text(index))

        return result
