from typing import List


class BoardSquare:
    def __init__(self):
        self.symbol: str = " "
        self.is_occupied = False

    def occupy(self, symbol: str):
        self.symbol = symbol
        self.is_occupied = True

    def __str__(self):
        return self.symbol

    def __repr__(self):
        return self.symbol


class Board:
    def __init__(self, size: int):
        self.squares: List[BoardSquare] = self.__create_board_squares(size)

    def __create_board_squares(self, board_size: int) -> List[BoardSquare]:
        squares = []
        for _ in range(0, board_size ** 2):
            squares.append(BoardSquare())
        return squares

    def occupy_square(self, index: int, symbol: str) -> bool:
        assert self.is_valid_square_index(index)

        if self.squares[index].is_occupied:
            return False

        self.squares[index].occupy(symbol)
        return True

    def is_square_occupied(self, index: int):
        assert self.is_valid_square_index(index)
        return self.squares[index].is_occupied

    def is_valid_square_index(self, index: int):
        return index in range(0, self.get_number_of_squares())

    def get_size(self):
        return int(len(self.squares) ** 0.5)

    def get_number_of_squares(self):
        return len(self.squares)

    def get_empty_squares_indexes(self):
        empty_squares: List[int] = []
        for i, square in enumerate(self.squares):
            if not square.is_occupied:
                empty_squares.append(i)

        return empty_squares

    def __str__(self):
        return str(self.squares)
