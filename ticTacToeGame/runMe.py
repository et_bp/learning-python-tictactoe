from cli.gameLoopCLI import GameLoopCLI
from player.player import Player
from cli.playersSetupCLI import ask_players_to_user
from player.playerFactory import PlayerFactory
from player.playerNameValidator import PlayerNameValidator, InvalidNameError
import click
from typing import List, Tuple


def validate_player_names(players_name) -> bool:
    try:
        PlayerNameValidator.validate_names(players_name)
    except InvalidNameError as e:
        print(
            f"Invalid player names: {e}. {PlayerNameValidator.describe_validation_rule()}"
        )
        return False
    return True


@click.command()
@click.argument("players_name", nargs=2, required=False)
def play_tic_tac_toe(players_name: Tuple[str, str] = ()):
    if len(players_name) == 0:
        players = PlayerFactory.create_unique_players(ask_players_to_user())
    else:
        if not validate_player_names(players_name):
            return
        players = PlayerFactory.create_unique_players(players_name)

    game_loop_command_line = GameLoopCLI()
    game_loop_command_line.start_game(players)


if __name__ == "__main__":
    play_tic_tac_toe()
