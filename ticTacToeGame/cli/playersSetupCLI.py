from player.player import Player
from player.playerFactory import PlayerFactory
from player.playerNameValidator import PlayerNameValidator
from typing import List, Tuple, Optional


def ask_players_to_user() -> List[str]:
    nb_players = 2

    player_names: List[str] = []
    for i in range(nb_players):
        player_names.append(ask_player_name(i, used_names=player_names))

    return player_names


def ask_player_name(index: int, used_names: List[str]) -> str:

    name_picked: str = ""
    while not name_picked:
        player_name = input(f"What is the name of Player {index + 1}? ")

        if not PlayerNameValidator.validate_name(player_name):
            print(
                f'"{player_name}" is not a valid name. {PlayerNameValidator.describe_validation_rule()}'
            )
            continue

        is_name_used = player_name in used_names
        if is_name_used:
            print(
                f'"{player_name}" is already picked by another player. Please pick a different name.'
            )
            continue

        name_picked = player_name

    return name_picked
