from player.player import Player
from game.gameSession import GameSession
from board.board import Board
from cli.boardCLI import BoardCLI
from inputs.inputmapping import TicTacToeInputMapping, InputNotMappedError
from typing import List


class GameLoopCLI:
    def __init__(self):
        self.board_view: BoardCLI = None
        self.input_mapping = TicTacToeInputMapping(
            {"1": 6, "2": 7, "3": 8, "4": 3, "5": 4, "6": 5, "7": 0, "8": 1, "9": 2}
        )

    def start_game(self, players: List[Player]):
        assert len(players) == 2

        self.__print_match_start(players)

        game_session: GameSession = GameSession(players)
        game_session.start()

        self.__setup_board_view(game_session.get_board())

        self.__execute_game_loop(game_session)

    def restart_game(self, players: List[Player]):
        self.board_view = None
        self.start_game(players)

    def end_session(self):
        pass

    def __execute_game_loop(self, game_session: GameSession):

        self.board_view.display()

        while not game_session.is_game_over():
            board_square_played: int = self.__ask_which_square_to_pick(game_session)
            game_session.try_make_move(board_square_played)
            self.board_view.display()

            if game_session.is_game_tied():
                self.__print_game_tied()
                break

            if game_session.is_game_won():
                self.__print_game_won(game_session.winner)
                break

        if self.__ask_restart_game():
            self.restart_game(game_session.players)
        else:
            self.__print_exit_message() 

    def __setup_board_view(self, board: Board):
        assert board
        assert self.board_view == None

        self.board_view = BoardCLI()
        self.board_view.board = board
        self.board_view.set_empty_squares_symbols(
            self.input_mapping.get_inputs_texts(
                [x for x in range(0, board.get_number_of_squares())]
            )
        )

    def __print_match_start(self, players: List[Player]):
        print(f"Tic-Tac-Toe game has begun!")

        players_description = ""
        for i, player in enumerate(players):
            players_description += f"{player.name} ('{player.symbol}')"
            if i < len(players) - 1:
                players_description += f" against "

        print(f"{players_description}\n")

    def __print_game_tied(self):
        print("It is a tie!")

    def __print_game_won(self, winner: Player):
        print(f"{winner} won the game! Congratulations!")

    def __print_enter_another_move(self, input: str, game_session: GameSession):
        print(
            f'"{input}" is not a valid place for a piece. Please enter one of these places {", ".join(self.__get_possible_input_texts(game_session))}'
        )

    def __ask_restart_game(self) -> bool:

        yes_inputs = ["yes", "y", "ye", "yup", "yep"]
        no_inputs = ["no", "n", "nope", "nee"]

        user_input = input(
            f'Would you like to restart the game? ("{yes_inputs[0]}" or "{no_inputs[0]}") '
        )

        while True:
            if user_input in yes_inputs:
                return True
            if user_input in no_inputs:
                return False
            else:
                user_input = input(
                    f'I don\'t understand "{user_input}". Please enter "{yes_inputs[0]}" or "{no_inputs[0]}": '
                )

        assert False

    def __ask_which_square_to_pick(self, game_session: GameSession) -> int:
        """ Asks the user which square to pick for the current player"""

        is_square_picked_valid = False
        square_picked_for_player = -1

        player = game_session.current_player
        while not is_square_picked_valid:

            input_from_user = input(f"{player}, where do you want to place a piece? ")

            try:
                square_picked_for_player = self.input_mapping.get_square_index(
                    input_from_user
                )
            except InputNotMappedError:
                self.__print_enter_another_move(input_from_user, game_session)
                continue

            if not game_session.is_move_possible(square_picked_for_player):
                self.__print_enter_another_move(input_from_user, game_session)
                continue

            is_square_picked_valid = True

        return square_picked_for_player

    def __get_possible_input_texts(self, game_session: GameSession):
        possible_input_texts: List[str] = []
        for possible_move in game_session.get_possible_moves():
            possible_input_texts.append(
                self.input_mapping.get_input_text(possible_move)
            )
        return possible_input_texts

    def __print_exit_message(self):
        print("Thanks for playing tic-tac-toe! See you next time!")
