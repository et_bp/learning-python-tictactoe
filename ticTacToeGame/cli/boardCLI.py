from board.board import Board, BoardSquare
from typing import List, Union
import copy


class BoardView:
    def __init__(self):
        self.board = None

    def display(self):
        pass


class BoardCLI(BoardView):
    def __init__(self):
        super().__init__()
        self.empty_squares_symbols = []

    def display(self):
        result: str = ""
        board_size = self.board.get_size()

        board_to_display: List[str] = self.__get_board_display_symbols()

        assert len(board_to_display) == self.board.get_number_of_squares()

        for i in range(0, board_size):

            result += self.__row(
                board_size,
                board_to_display[i * board_size : i * board_size + board_size :],
            )

            if i < board_size - 1:
                result += self.__horizontal_dividers(board_size)

        print(result)

    def set_empty_squares_symbols(self, symbols: List[str]):
        """
        Set the symbols that should be displayed if a square is empty
        Inputs:
            symbols: Each string will map to a square in the board.  
                     For example, str at index 0 will be placed at index 0 in the board if the index is not occupied. 
        """
        self.empty_squares_symbols = symbols

    def __get_board_display_symbols(self) -> List[str]:
        """
        Get the symbols that should be in all the squares of the board to display
        This includes the symbols in the board (when they are filled in) or the default symbols from the
        "self.empty_squares_symbols".
        """
        final_squares: List[str] = []
        for i, square in enumerate(self.board.squares):
            final_squares.append(square.symbol)
            if not square.is_occupied and len(self.empty_squares_symbols) - 1 >= i:
                final_squares[i] = f"({self.empty_squares_symbols[i]})"
        return final_squares

    def __horizontal_divider(self) -> str:
        return "-------"

    def __horizontal_dividers(self, num: int) -> str:
        horizontal_dividers: str = ""
        for _ in range(0, num):
            horizontal_dividers += f"{self.__horizontal_divider()} "
        return horizontal_dividers + "\n"

    def __row(self, num: int, squares_symbols: List[str]) -> str:
        assert len(squares_symbols) == num

        nb_white_spaces = len(self.__horizontal_divider())
        nb_vertical_symbol: int = int(len(self.__horizontal_divider()) / 2)

        vertical_dividers: str = ""
        for i in range(0, nb_vertical_symbol):
            for j in range(0, num):

                vertical_separator = "|" if j < num - 1 else ""

                if i == int(nb_vertical_symbol / 2):
                    centered_square_symbol = squares_symbols[j].center(
                        nb_white_spaces, " "
                    )
                    one_line = f"{centered_square_symbol}"
                else:
                    one_line = [" "] * nb_white_spaces

                one_line += vertical_separator

                vertical_dividers += "".join(one_line)

            vertical_dividers += "\n"
        return vertical_dividers


if __name__ == "__main__":
    # Test the BoardCommandLineView
    board_view_to_test = BoardCLI()
    board_view_to_test.board = Board(3)

    print("Display an Empty Board")
    board_view_to_test.display()

    board_view_to_test.board.occupy_square(0, "X")
    board_view_to_test.board.occupy_square(1, "X")
    board_view_to_test.board.occupy_square(3, "X")

    print("Display a board with [0, 1, 3] occupied")
    board_view_to_test.display()

    board_view_to_test.set_empty_squares_symbols(
        [x for x in range(0, board_view_to_test.board.get_number_of_squares())]
    )

    print("Display a board with [0, 1, 3] occupied and empty squares symbols")
    board_view_to_test.display()
