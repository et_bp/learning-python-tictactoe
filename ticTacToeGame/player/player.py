class Player:
    def __init__(self, name: str, symbol: str):
        self.name = name
        self.symbol = symbol

    def __str__(self):
        return self.name

    def __repr__(self):
        return f"{self.name}({self.symbol})"

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.name == other.name and self.symbol == other.symbol
        return False
