from typing import List


class InvalidNameError(Exception):
    pass


class PlayerNameValidator:
    @staticmethod
    def validate_name(name: str) -> bool:
        return name.isalpha()

    @staticmethod
    def validate_names(names: List[str]) -> bool:
        invalid_names = []
        for name in names:
            if not PlayerNameValidator.validate_name(name):
                invalid_names.append(name)

        if len(invalid_names) > 0:
            raise InvalidNameError(f"{' '.join(invalid_names)}")
        else:
            return True

    @staticmethod
    def describe_validation_rule() -> str:
        return "A name must only contain letters from the alphabet. (No numbers or special characters allowed)"
