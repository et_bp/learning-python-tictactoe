from player.player import Player
from typing import List, Dict


class PlayerFactory:
    @staticmethod
    def create_player(player_name: str) -> Player:
        return Player(player_name, player_name[0])

    @staticmethod
    def create_unique_players(players_name: List[str]) -> List[Player]:
        return PlayerFactory.__make_players_unique(
            PlayerFactory.__create_players(players_name)
        )

    @staticmethod
    def __create_players(players_name: List[str]) -> List[Player]:
        return [PlayerFactory.create_player(name) for name in players_name]

    @staticmethod
    def __make_players_unique(players: List[Player]) -> List[Player]:
        unique_players = []
        nb_same_players_per_symbol: Dict[str, int] = {}
        for player in players:
            if player.symbol in nb_same_players_per_symbol:
                unique_players.append(
                    Player(
                        player.name,
                        f"{player.symbol}{nb_same_players_per_symbol[player.symbol]}",
                    )
                )
                nb_same_players_per_symbol[player.symbol] += 1
            else:
                unique_players.append(Player(player.name, player.symbol))
                nb_same_players_per_symbol.setdefault(player.symbol, 1)

        return unique_players
