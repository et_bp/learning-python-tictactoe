import unittest
from board.board import Board, BoardSquare


class TestBoardSquare(unittest.TestCase):
    def setUp(self):
        self.board_square = BoardSquare()

    def tearDown(self):
        del self.board_square

    def test_occupy(self):

        symbol = "X"

        self.board_square.occupy(symbol)
        self.assertTrue(self.board_square.is_occupied)
        self.assertEqual(self.board_square.symbol, symbol)


class TestBoard(unittest.TestCase):
    def setUp(self):
        self.board_size = 3
        self.board = Board(size=self.board_size)

    def tearDown(self):
        del self.board

    def test_occupy_square(self):
        index = 0
        symbol = "X"

        self.assertTrue(self.board.occupy_square(index, symbol))
        self.assertEqual(self.board.is_square_occupied(index), True)

    def test_already_occupied_square(self):
        index = 0
        symbol = "X"

        self.assertTrue(self.board.occupy_square(index, symbol))
        self.assertFalse(self.board.occupy_square(index, symbol))

    def test_indexes_out_of_range(self):
        indexes_out_of_range = [100, -1, 9, 100000]
        symbol = "X"

        for index in indexes_out_of_range:
            self.assertFalse(self.board.is_valid_square_index(index))
            self.assertRaises(AssertionError, self.board.occupy_square, index, symbol)

    def test_board_size(self):
        self.assertTrue(self.board.get_size(), self.board_size)
        self.assertTrue(self.board.get_number_of_squares, self.board_size ** 2)

    def test_get_empty_squares(self):
        squares_to_occupy = [0, 1, 2, 8]
        squares_should_be_empty = [3, 4, 5, 6, 7]

        for index in squares_to_occupy:
            self.board.occupy_square(index, "X")

        self.assertEqual(
            squares_should_be_empty, self.board.get_empty_squares_indexes()
        )


if __name__ == "__main__":
    unittest.main()
