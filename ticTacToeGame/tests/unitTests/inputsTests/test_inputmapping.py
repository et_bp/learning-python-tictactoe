import unittest
from inputs.inputmapping import TicTacToeInputMapping, InputNotMappedError
from collections import namedtuple
from parameterized import parameterized


class TestTicTacToeInputMapping(unittest.TestCase):
    def setUp(self):
        self.input_mapping = TicTacToeInputMapping(
            {"1": 0, "2": 1, "3": 2, "4": 3, "5": 4, "6": 5, "7": 6, "8": 7, "9": 8}
        )

    def tearDown(self):
        del self.input_mapping

    @parameterized.expand(
        [
            ("top right", "1", 0),
            ("middle", "5", 4),
            ("bottom left", "7", 6),
            ("bottom right", "9", 8),
        ]
    )
    def test_get_square_index(self, test_name, input_text, expected_square):
        self.assertEqual(
            self.input_mapping.get_square_index(input_text), expected_square
        )

    @parameterized.expand(
        [
            ("letter", "a"),
            ("special character", "@"),
            ("large number", "100"),
            ("under min", "0"),
            ("over max", "10"),
        ]
    )
    def test_get_square_index_fail(self, test_name, input_text):
        self.assertRaises(
            InputNotMappedError, self.input_mapping.get_square_index, input_text
        )

    @parameterized.expand(
        [
            ("top left", 0, "1"),
            ("middle", 4, "5"),
            ("bottom left", 6, "7"),
            ("bottom right", 8, "9"),
        ]
    )
    def test_get_input_text(self, test_name, square_index, expected_input_text):
        self.assertEqual(
            self.input_mapping.get_input_text(square_index), expected_input_text
        )

    @parameterized.expand(
        [
            ("big negative", -1000),
            ("under min", -1),
            ("over max", 10),
            ("big positive", 1000),
        ]
    )
    def test_get_input_text_fail(self, test_name, bad_square_index):
        self.assertRaises(
            InputNotMappedError, self.input_mapping.get_input_text, bad_square_index
        )

    def test_get_all_input_texts(self):
        result = [str(x) for x in range(1, 10)]
        self.assertEqual(result, self.input_mapping.get_all_input_texts())

    def test_get_inputs_texts(self):
        input = [x for x in range(0, 9)]
        result = [str(x) for x in range(1, 10)]

        self.assertEqual(result, self.input_mapping.get_inputs_texts(input))


if __name__ == "__main__":
    unittest.main()
