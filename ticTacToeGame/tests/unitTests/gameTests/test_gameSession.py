import unittest
from game.gameSession import GameSession
from player.player import Player
from typing import List, Tuple
from parameterized import parameterized


class TestGameSession(unittest.TestCase):
    def setUp(self):
        self.players: List[Player] = [
            Player("TestPlayer1", "T1"),
            Player("TestPlayer2", "T2"),
        ]

        self.game_session = GameSession(self.players)

    def tearDown(self):
        del self.players
        del self.game_session

    def test_start(self):
        self.assertIsNotNone(self.game_session.active_game)
        self.assertIsNotNone(self.game_session.current_player)

    def test_try_make_move_success(self):

        self.assertEqual(self.players[0], self.game_session.current_player)

        self.assertTrue(self.game_session.try_make_move(0))
        self.assertTrue(self.game_session.active_game.board.is_square_occupied(0))

        self.assertEqual(self.players[1], self.game_session.current_player)

    def test_try_make_move_already_occupied(self):
        self.assertTrue(self.game_session.try_make_move(0))
        self.assertFalse(self.game_session.try_make_move(0))

        self.assertEqual(self.players[1], self.game_session.current_player)

    def test_try_make_move_out_of_bound(self):
        nb_squares = self.game_session.get_board().get_number_of_squares()
        moves_out_of_bound = [nb_squares, -1, nb_squares + 100]

        for move in moves_out_of_bound:
            self.assertRaises(AssertionError, self.game_session.try_make_move, move)

    def test_get_possible_moves(self):
        moves_to_do: List[int] = [0, 2, 5, 8]

        nb_squares = self.game_session.get_board().get_number_of_squares()
        possible_moves: List[int] = [
            x for x in range(nb_squares) if x not in moves_to_do
        ]

        for move in moves_to_do:
            self.assertTrue(self.game_session.try_make_move(move))

        self.assertEqual(possible_moves, self.game_session.get_possible_moves())

    @parameterized.expand([
        ("with 1st horizontal line", [0, 1, 2], [3, 6, 8]),
        ("with 2nd horizontal line", [3, 4, 5], [0, 2, 8]),
        ("with 3rd horizontal line", [6, 7, 8], [1, 2, 5]),
        ("with 1st vertical line", [0, 3, 6], [1, 2, 8]),
        ("with 2nd vertical line", [1, 4, 7], [0, 2, 8]),
        ("with 3rd vertical line", [2, 5, 8], [0, 1, 3]),
        ("with top-left to bottom-right diagonal", [0, 4, 8], [1, 2, 7]),
        ("with top-right to bottom-left diagonal", [2, 4, 6], [0, 1, 3]),
        ("on last turn", [0, 1, 5, 7, 2], [3, 4, 6, 8])
    ])
    def test_is_game_won(self, test_name, moves_player1, moves_player2):
        # The first player should always win, the second has random moves
        def assert_won(game_session: GameSession, player: Player):
            self.assertTrue(
                game_session.is_game_won(),
                msg=f"Game should be won but it wasn't. Current Player is {game_session.current_player}. State of the game is {game_session.get_board()}",
            )
            self.assertEqual(
                self.game_session.winner,
                player,
                msg=f"{game_session.winner} won, but {player} was expected to win!",
            )

        def assert_not_won(game_session: GameSession):
            self.assertFalse(
                game_session.is_game_won(),
                msg=f"Game should not be won, but it was. Current Player is {game_session.current_player}. State of the game is {game_session.get_board()}",
            )

        nb_moves = len(moves_player1)
        for i in range(nb_moves):
            # play one full turn (all players)
            self.game_session.try_make_move(moves_player1[i])

            # first player should always win on his last move
            if i == nb_moves - 1:
                assert_won(self.game_session, self.players[0])
                break
            else:
                assert_not_won(self.game_session)

                self.game_session.try_make_move(moves_player2[i])
                # 2nd player should never win in this test
                assert_not_won(self.game_session)

        self.tearDown()
        self.setUp()

    @parameterized.expand(
        [
            ("tied #1", [6, 0, 5, 7, 2], [4, 3, 1, 8]),
            ("tied #2", [1, 2, 8, 3, 7], [4, 0, 5, 6]),
        ]
    )
    def test_is_game_tied(
        self, test_name, moves_player1: List[int], moves_player2: List[int]
    ):
        nb_moves_player1 = len(moves_player1)
        nb_moves_player2 = len(moves_player2)

        assert (
            nb_moves_player1 + nb_moves_player2
            == self.game_session.get_total_number_of_moves()
        )
        assert nb_moves_player1 == nb_moves_player2 + 1

        number_of_turns = nb_moves_player1
        for turn_count in range(number_of_turns):
            self.assertTrue(self.game_session.try_make_move(moves_player1[turn_count]))
            # On the last turn, the 2nd player can't play since there is no moves left
            if turn_count < nb_moves_player2:
                self.assertTrue(
                    self.game_session.try_make_move(moves_player2[turn_count])
                )

            # Keep validating the game is not won
            self.assertFalse(self.game_session.is_game_won(), f"{self.game_session.winner} won the game on turn {turn_count}. This should have been a tie.")
        
        self.assertEqual(len(self.game_session.get_possible_moves()), 0)
        self.assertTrue(self.game_session.is_game_tied())


if __name__ == "__main__":
    unittest.main()
