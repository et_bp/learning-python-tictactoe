import unittest
from board.board import Board
from cli.boardCLI import BoardCLI
from typing import List


class TestBoardCommandLineView(unittest.TestCase):
    def setUp(self):
        self.board_view = BoardCLI()
        self.board_view.board = Board(3)

    def tearDown(self):
        del self.board_view

    def fill_empty_squares_symbols(self, list_to_fill, empty_squares_symbol):
        for i in range(0, len(list_to_fill)):
            if i > len(empty_squares_symbol) - 1:
                break
            else:
                list_to_fill[i] = f"({empty_squares_symbol[i]})"

    def test_get_board_display_symbols_no_empty(self):
        symbol = "X"
        self.board_view.board.occupy_square(0, symbol)

        self.assertEqual(
            [symbol, " ", " ", " ", " ", " ", " ", " ", " "],
            self.board_view._BoardCLI__get_board_display_symbols(),
        )

    def test_get_board_display_symbols_with_empty(self):
        result: List[str] = [" "] * self.board_view.board.get_number_of_squares()
        empty_squares_symbols = ["A", "B", "C"]
        self.fill_empty_squares_symbols(result, empty_squares_symbols)

        self.board_view.set_empty_squares_symbols(empty_squares_symbols)
        self.assertEqual(result, self.board_view._BoardCLI__get_board_display_symbols())

    def test_get_board_display_symbols_with_empty_and_occupy(self):
        result: List[str] = [" "] * self.board_view.board.get_number_of_squares()
        empty_squares_symbols = ["A", "B", "C", "D", "E", "F", "G", "H", "I"]
        self.fill_empty_squares_symbols(result, empty_squares_symbols)

        self.board_view.board.occupy_square(0, "X")
        result[0] = "X"

        self.board_view.set_empty_squares_symbols(empty_squares_symbols)

        self.assertEqual(result, self.board_view._BoardCLI__get_board_display_symbols())


if __name__ == "__main__":
    unittest.main()
