import unittest
from parameterized import parameterized
from player.playerNameValidator import PlayerNameValidator, InvalidNameError


class TestPlayerNameValidator(unittest.TestCase):
    @parameterized.expand(
        [
            ("valid name 1", "Bob", True),
            ("valid name 2", "Zephyr", True),
            ("with number", "Bob1", False),
            ("with special character", "@Bob", False),
            ("empty name", "", False),
        ]
    )
    def test_validate_name(self, test_name, name: str, is_name_valid: bool):
        self.assertEqual(
            PlayerNameValidator.validate_name(name),
            is_name_valid,
            f"Validate name '{name}' should return {is_name_valid}",
        )

    @parameterized.expand([("valid names", ["Bob", "Zephyr", "Aragorn"])])
    def test_validate_names(self, test_name, names_to_validate):
        self.assertTrue(PlayerNameValidator.validate_names(names_to_validate))

    @parameterized.expand(
        [
            ("one_invalid_name", ["Bob", "@Bob", "Aragorn"], ["@Bob"]),
            (
                "all_invalid_names",
                ["1Bob", "@Zephyr", "!Aragorn"],
                ["1Bob", "@Zephyr", "!Aragorn"],
            ),
        ]
    )
    def test_validate_invalid_names(self, test_name, names, invalid_names):
        self.assertRaisesRegex(
            InvalidNameError,
            f"{' '.join(invalid_names)}",
            PlayerNameValidator.validate_names,
            names,
        )
