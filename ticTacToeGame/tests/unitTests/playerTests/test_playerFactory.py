import unittest
from player.player import Player
from player.playerFactory import PlayerFactory
from parameterized import parameterized
from typing import List


class TestPlayerFactory(unittest.TestCase):
    @parameterized.expand(
        [("Bob", "Bob", Player("Bob", "B")), ("Anita", "Anita", Player("Anita", "A"))]
    )
    def test_create_player(self, test_name, player_name, expected_player) -> Player:
        created_player = PlayerFactory.create_player(player_name)
        self.assertEqual(created_player, expected_player)

    @parameterized.expand(
        [
            (
                "Same First Letter",
                ["Bob", "Bobby"],
                [Player("Bob", "B"), Player("Bobby", "B1")],
            )
        ]
    )
    def test_create_unique_players(
        self, test_name, players_name: List[str], expected_players: List[Player]
    ) -> List[Player]:
        created_players = PlayerFactory.create_unique_players(players_name)
        self.assertListEqual(created_players, expected_players)
