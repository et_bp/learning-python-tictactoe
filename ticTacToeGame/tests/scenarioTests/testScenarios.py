import unittest
from tests.scenarioTests.scenariosHelperLibrary import Player, ScenarioSteps, ExpectedLines


class TestScenarios(unittest.TestCase):
    def setUp(self):
        self.player1 = Player("PlayerOne", "P")
        self.player2 = Player("PlayerTwo", "P1")
        self.timeout = 5
        self.tic_tac_toe_game = ScenarioSteps.launch_tictactoe_for_test(self.timeout)

    def tearDown(self):
        ScenarioSteps.exit_tictactoe(self.tic_tac_toe_game)

    def test_first_player_wins(self):
        ScenarioSteps.test_give_players_names(self.tic_tac_toe_game, self.player1, self.player2)
        ScenarioSteps.test_players_names_shown(self.tic_tac_toe_game, self.player1, self.player2)
        ScenarioSteps.test_player_1_wins(self.tic_tac_toe_game, self.player1, self.player2)
        ScenarioSteps.test_decline_restart_game(self.tic_tac_toe_game)

    def test_first_player_wins_on_last_turn(self):
        ScenarioSteps.test_give_players_names(self.tic_tac_toe_game, self.player1, self.player2)
        ScenarioSteps.test_players_names_shown(self.tic_tac_toe_game, self.player1, self.player2)
        ScenarioSteps.test_player_1_wins_on_last_turn(self.tic_tac_toe_game, self.player1, self.player2)
        ScenarioSteps.test_decline_restart_game(self.tic_tac_toe_game)

    def test_tie(self):
        ScenarioSteps.test_give_players_names(self.tic_tac_toe_game, self.player1, self.player2)
        ScenarioSteps.test_players_names_shown(self.tic_tac_toe_game, self.player1, self.player2)
        ScenarioSteps.test_tie(self.tic_tac_toe_game, self.player1, self.player2)
        ScenarioSteps.test_decline_restart_game(self.tic_tac_toe_game)
