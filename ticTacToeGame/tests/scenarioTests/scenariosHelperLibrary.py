from pexpect.popen_spawn import PopenSpawn, signal

class Player:
    def __init__(self, name, symbol):
        self.name = name
        self.symbol = symbol


class ExpectedLines:
    @staticmethod
    def what_is_player_name(player_count):
        return f"What is the name of Player {player_count}?"

    @staticmethod
    def show_players_in_game(player1: Player, player2: Player):
        return f"{player1.name} \('{player1.symbol}'\).*{player2.name} \('{player2.symbol}'\)"

    @staticmethod
    def ask_move_to_player(player):
        return f"{player.name}.*?"

    @staticmethod
    def game_is_won_by_player(player: Player):
        return f"{player.name}.* won"

    @staticmethod
    def ask_restart_new_game():
        return f"restart.*\?"

    @staticmethod
    def game_is_tied():
        return f"tie"


class ScenarioSteps:
    @staticmethod
    def launch_tictactoe_for_test(test_timeout) -> PopenSpawn:
        return PopenSpawn("pipenv run python .\\ticTacToeGame\\runMe.py", timeout=test_timeout)

    @staticmethod
    def exit_tictactoe(game_process: PopenSpawn):
        # ToRefactor: this fires a "ResourceWarning: unclosed file". How can this be avoided?
        game_process.kill(signal.SIGILL)
        game_process.wait()

    @staticmethod
    def test_give_players_names(
        game_process: PopenSpawn, player1: Player, player2: Player
    ):
        game_process.expect(ExpectedLines.what_is_player_name(1))
        game_process.sendline(player1.name)

        game_process.expect(ExpectedLines.what_is_player_name(2))
        game_process.sendline(player2.name)

    @staticmethod
    def test_player_1_wins(game_process: PopenSpawn, player1: Player, player2: Player):
        game_process.expect(ExpectedLines.ask_move_to_player(player1))
        game_process.sendline("1")

        game_process.expect(ExpectedLines.ask_move_to_player(player2))
        game_process.sendline("3")

        game_process.expect(ExpectedLines.ask_move_to_player(player1))
        game_process.sendline("4")

        game_process.expect(ExpectedLines.ask_move_to_player(player2))
        game_process.sendline("6")

        game_process.expect(ExpectedLines.ask_move_to_player(player1))
        game_process.sendline("7")

        game_process.expect(ExpectedLines.game_is_won_by_player(player1))

    @staticmethod
    def test_player_1_wins_on_last_turn(game_process: PopenSpawn, player1: Player, player2: Player):
        game_process.expect(ExpectedLines.ask_move_to_player(player1))
        game_process.sendline("7")

        game_process.expect(ExpectedLines.ask_move_to_player(player2))
        game_process.sendline("8")

        game_process.expect(ExpectedLines.ask_move_to_player(player1))
        game_process.sendline("4")

        game_process.expect(ExpectedLines.ask_move_to_player(player2))
        game_process.sendline("6")

        game_process.expect(ExpectedLines.ask_move_to_player(player1))
        game_process.sendline("5")

        game_process.expect(ExpectedLines.ask_move_to_player(player2))
        game_process.sendline("3")

        game_process.expect(ExpectedLines.ask_move_to_player(player1))
        game_process.sendline("9")

        game_process.expect(ExpectedLines.ask_move_to_player(player2))
        game_process.sendline("2")

        game_process.expect(ExpectedLines.ask_move_to_player(player1))
        game_process.sendline("1")

        game_process.expect(ExpectedLines.game_is_won_by_player(player1))

    @staticmethod
    def test_tie(game_process: PopenSpawn, player1: Player, player2: Player):
        game_process.expect(ExpectedLines.ask_move_to_player(player1))
        game_process.sendline("7")

        game_process.expect(ExpectedLines.ask_move_to_player(player2))
        game_process.sendline("8")

        game_process.expect(ExpectedLines.ask_move_to_player(player1))
        game_process.sendline("5")

        game_process.expect(ExpectedLines.ask_move_to_player(player2))
        game_process.sendline("4")

        game_process.expect(ExpectedLines.ask_move_to_player(player1))
        game_process.sendline("6")

        game_process.expect(ExpectedLines.ask_move_to_player(player2))
        game_process.sendline("9")

        game_process.expect(ExpectedLines.ask_move_to_player(player1))
        game_process.sendline("1")

        game_process.expect(ExpectedLines.ask_move_to_player(player2))
        game_process.sendline("3")

        game_process.expect(ExpectedLines.ask_move_to_player(player1))
        game_process.sendline("2")

        game_process.expect(ExpectedLines.game_is_tied())

    @staticmethod
    def test_players_names_shown(
        game_process: PopenSpawn, player1: Player, player2: Player
    ):
        game_process.expect(ExpectedLines.show_players_in_game(player1, player2))

    @staticmethod
    def test_decline_restart_game(game_process: PopenSpawn):
        game_process.expect(ExpectedLines.ask_restart_new_game())
        game_process.sendline("no")
