from game.game import Game
from board.board import Board, BoardSquare
from player.player import Player
from typing import List, Optional


class GameSession:
    """
    Class implementing all the actions that can be done in a tic-tac-toe game by the players.
    It is the main entry point the change the state of the game.
    """

    def __init__(self, players: List[Player]):
        """
        Create a GameSession with exactly 2 players (AssertionError will be thrown otherwise)
        As soon as self is initialized, the game will be started.
        """

        assert len(players) == 2
        self.players = players
        self.active_game: Game = None

        # by default, start the game since all information needed is acquired
        self.start()

    def start(self):
        """
        Start the game. The first player is always the first one in the list provided.
        """
        if self.active_game:
            return

        self.active_game = Game(self.players)
        self.current_player = self.players[0]

        self.winner: Player = None

    def try_make_move(self, index: int) -> bool:
        """
        Try to make a move for the current player. If the move is successful, the current player changes.
        Returns false if the move can't be done in the current board
        """

        assert self.active_game

        if not self.active_game.board.occupy_square(index, self.current_player.symbol):
            return False

        self.__update_game_won()

        self.__switch_player()
        return True

    def is_move_possible(self, index: int) -> bool:
        assert self.active_game

        return self.active_game.board.is_valid_square_index(
            index
        ) and not self.active_game.board.is_square_occupied(index)

    def get_possible_moves(self) -> List[int]:
        assert self.active_game

        return self.active_game.board.get_empty_squares_indexes()

    def get_board(self):
        if not self.active_game:
            return None

        return self.active_game.board

    def get_total_number_of_moves(self):
        assert self.get_board() != None
        return self.get_board().get_number_of_squares()

    def is_game_won(self):
        """
        Check if the game is won in its current state
        """
        return self.winner != None

    def is_game_tied(self):
        no_more_moves_left = len(self.get_possible_moves()) == 0
        game_isnt_won = not self.is_game_won()
        return game_isnt_won and no_more_moves_left

    def is_game_over(self):
        return self.is_game_won() or self.is_game_tied()

    def __update_game_won(self):
        winning_symbol = self.__get_winning_symbol()
        if winning_symbol:
            self.winner = self.__get_player(winning_symbol)

    def __get_winning_symbol(self) -> Optional[str]:
        """ Get the symbol that is won the game or None if no one is winning"""
        board_size = self.active_game.board.get_size()
        board_squares = self.active_game.board.squares
        nb_squares = len(board_squares)

        top_left_diag = slice(0, nb_squares, board_size + 1)
        top_right_diag = slice(
            board_size - 1, (board_size - 1) * board_size + 1, board_size - 1
        )
        horizontal_lines = [
            slice(x, x + board_size) for x in range(0, nb_squares, board_size)
        ]
        vertical_lines = [
            slice(x, nb_squares, board_size) for x in range(0, board_size)
        ]

        lines_to_check: List[slice] = []
        lines_to_check.append(top_left_diag)
        lines_to_check.append(top_right_diag)
        lines_to_check.extend(horizontal_lines)
        lines_to_check.extend(vertical_lines)

        for line in lines_to_check:
            squares_to_check: List[BoardSquare] = board_squares[line]
            if self.__are_squares_winning(squares_to_check):
                return squares_to_check[0].symbol

    def __are_squares_winning(self, squares: List[BoardSquare]) -> bool:
        """
        Is the list of squares a winning sequence. Means that all squares are played by the same player
        """
        if len(squares) == 0:
            return False

        if not squares[0].is_occupied:
            return False

        winning_symbol = squares[0].symbol
        for square in squares:
            if not square.is_occupied or winning_symbol != square.symbol:
                return False

        return True

    def __switch_player(self):
        current_player_index = self.players.index(self.current_player)

        next_index = current_player_index + 1
        if next_index == len(self.players):
            next_index = 0

        self.current_player = self.players[next_index]

    def __get_player(self, symbol: str) -> Player:
        return next(filter(lambda player: player.symbol == symbol, self.players), None)
