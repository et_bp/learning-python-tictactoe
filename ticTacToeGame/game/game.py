from board.board import Board
from player.player import Player
from typing import List


class Game:
    def __init__(self, players: List[Player]):

        assert len(players) == 2

        self.players = players
        self.board = Board(3)
